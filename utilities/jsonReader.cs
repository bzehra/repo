﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumFramework.utilities
{
    public class jsonReader
    {
        public string extractData(String tokenName)
        {
            String myJsonString = File.ReadAllText("utilities/testData.json");

            var jsonObject = JToken.Parse(myJsonString);

            return jsonObject.SelectToken(tokenName).Value<String>();
        }

        public string[] extractDataArray(String tokenName)
        {
            String myJsonString = File.ReadAllText("utilities/testData.json");

            var jsonObject = JToken.Parse(myJsonString);

            IList<String> productsList = jsonObject.SelectTokens(tokenName).Values<String>().ToList();

            return productsList.ToArray();
        }
    }
}
