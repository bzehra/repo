﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumFramework.pageObjects
{
    class confirmationPage
    {
        private IWebDriver driver;

        By location = By.CssSelector("#country");
        By termsChkBox = By.CssSelector("label[for='checkbox2']");
        By purchaseBtn = By.CssSelector("input[value='Purchase']");
        By actualMessage = By.CssSelector(".alert-success");

        public confirmationPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement getLocationTxtbox()
        {
            return driver.FindElement(location);
        }


        public void acceptTermsAndConditions()
        {
            driver.FindElement(termsChkBox).Click();
        }

        public void selectLocation(String location)
        {
            //Explicit Wait
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText(location)));
            driver.FindElement(By.LinkText(location)).Click();

        }

        public void purchase()
        {
            driver.FindElement(purchaseBtn).Click();
        }

        public IWebElement getActualMessage()
        {
            return driver.FindElement(actualMessage);
        }
    }
}
