﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumFramework.pageObjects
{
    class ProductsPage
    {
        private IWebDriver driver;

        By allCards = By.TagName("app-card");
        By cardTitle = By.XPath("div/div[1]/h4/a");
        By addToCardBtn = By.CssSelector(".card-footer button");
        By checkoutBtn = By.PartialLinkText("Checkout");

        public ProductsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IList<IWebElement> getAllCards()
        {
            return driver.FindElements(allCards);
        }

        public By getCardTitle()
        {
            return cardTitle;
        }

        public By getAddToCartBtn()
        {
            return addToCardBtn;
        }

        public checkoutPage clickCheckoutBtn()
        {
            driver.FindElement(checkoutBtn).Click();
            checkoutPage cp = new checkoutPage(driver);
            return cp;
        }

        public void waitUntilPageLoads()
        {
            //Explicit Wait
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.PartialLinkText("Checkout")));

        }



    }
}
