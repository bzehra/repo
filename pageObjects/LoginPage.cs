﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumFramework.pageObjects
{
    class LoginPage
    {
        private IWebDriver driver;

        By username = By.Id("username");
        By password = By.Id("password");
        By terms = By.CssSelector("#terms");
        By signInBtn = By.XPath("//input[@name='signin']");

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement getUsername()
        {
            return driver.FindElement(username);
        }

        public IWebElement getPassword()
        {
            return driver.FindElement(password);
        }

        public IWebElement getTermsChkBox()
        {
            return driver.FindElement(terms);
        }

        public IWebElement getSignInBtn()
        {
            return driver.FindElement(signInBtn);
        }

        public ProductsPage validLogin(String user, String pass)
        {
            driver.FindElement(username).SendKeys(user);
            driver.FindElement(password).SendKeys(pass);
            driver.FindElement(terms).Click();
            driver.FindElement(signInBtn).Click();
            return new ProductsPage(driver);

        }
    }
}
