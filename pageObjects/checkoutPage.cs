﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumFramework.pageObjects
{
    class checkoutPage
    {
        private IWebDriver driver;

        By checkoutCards = By.CssSelector("h4 a");
        By checkoutBtn = By.CssSelector(".btn-success");

        public checkoutPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IList<IWebElement> getCards()
        {
            return driver.FindElements(checkoutCards);
        }

        public confirmationPage checkOut()
        {
            driver.FindElement(checkoutBtn).Click();
            confirmationPage locPage = new confirmationPage(driver);
            return locPage;
        }
    }
}
