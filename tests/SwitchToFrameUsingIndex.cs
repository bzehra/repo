﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDriverManager.DriverConfigs.Impl;

namespace CSharpSeleniumFramework
{
    //[Parallelizable(ParallelScope.Self)]
    class SwitchToFrameUsingIndex
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()

        {
            new WebDriverManager.DriverManager().SetUpDriver(new ChromeConfig());
            driver = new ChromeDriver();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            driver.Manage().Window.Maximize();
            driver.Url = "https://rahulshettyacademy.com/AutomationPractice/";
        }

        [Test]
        public void test_frameByID()
        {

            //Note: Frames can we identified using ID, name and index
            IWebElement frame = driver.FindElement(By.Id("courses-iframe"));
             IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", frame);

            driver.SwitchTo().Frame("courses-iframe");

            Thread.Sleep(3000);
            driver.FindElement(By.LinkText("All Access Plan")).Click();

            TestContext.Progress.WriteLine(driver.FindElement(By.CssSelector("h1")).Text);

            driver.SwitchTo().DefaultContent();

            TestContext.Progress.WriteLine(driver.FindElement(By.CssSelector("h1")).Text);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }
    }
}
