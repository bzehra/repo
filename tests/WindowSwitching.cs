﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDriverManager.DriverConfigs.Impl;

namespace CSharpSeleniumFramework
{
    //[Parallelizable(ParallelScope.Self)]
    class WindowSwitching
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()

        {
            new WebDriverManager.DriverManager().SetUpDriver(new ChromeConfig());
            driver = new ChromeDriver();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            driver.Manage().Window.Maximize();
            driver.Url = "https://rahulshettyacademy.com/loginpagePractise/";
        }

        [Test]
        public void test_parentchildwindowswitching()
        {
            //Thread.Sleep(3000);
            driver.FindElement(By.CssSelector(".blinkingText")).Click();
            IList<String> windowsIDs = driver.WindowHandles;
            Assert.AreEqual(2, windowsIDs.Count);
            driver.SwitchTo().Window(windowsIDs[1]);

            TestContext.Progress.WriteLine(driver.FindElement(By.CssSelector(".red")).Text);
            String text = driver.FindElement(By.CssSelector(".red")).Text;
            String[] splitText = text.Split("at");
            String[] splittext2 = splitText[1].Split("with");
            String finalText = splittext2[0].Trim();

            driver.SwitchTo().Window(windowsIDs[0]);

            driver.FindElement(By.Id("username")).SendKeys(finalText);

        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }

    }
}
