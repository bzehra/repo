using CSharpSeleniumFramework.pageObjects;
using CSharpSeleniumFramework.utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDriverManager.DriverConfigs.Impl;

namespace CSharpSeleniumFramework
{
    [Parallelizable(ParallelScope.Self)]
    public class E2ETest : Base
    {

        [Test,TestCaseSource("AddTestcaseDataConfig"), Category("Smoke")]
        /* [TestCase("rahulshettyacademy", "learning", "India")]
         [TestCase("rahulshetty", "learning", "India")]*/

        //run all data sets of test method in parallel: write "[Parallelizable(ParallelScope.All)]" before the test method
        //run all test methods in one class parallel: write "[Parallelizable(ParallelScope.Children)]" before the class declaration
        //run all test files in project parallel: write "[Parallelizable(ParallelScope.Self)]" before the declaration of each class in project 
        //For running all/multiple Test Method in one class in parallel, the below code will be valid if that class is the only class which is marked as "[Parallelizable(ParallelScope.Self)]" in its project
        //[Parallelizable(ParallelScope.Self)] - In Class Level

        //dotnet test pathto.csproj (Enter this in terminal to run all tests in the project)
        //dotnet test pathto.csproj --filter TestCategory=Smoke (Enter this in terminal to run all tests in the project with a specific category)
        //dotnet test CSharpSeleniumFramework.csproj --filter TestCategory=Smoke -- TestRunParameters.Parameter(name=\"browserName\",value=\"Chrome\")
        //(Enter above cmmand to pass browserName as argument from COMMAND PROMPT as I had issues with running this through powershell)

        //The below test will cause the tests in this CLASS to run in sequential order and
        //and run the TEST WITH MULTIPLE DATA SETS in parallel
        [Parallelizable(ParallelScope.All)]
        public void EndToEndFlow(String username, String password, String[] expProducts, String location)
        {
            //String[] expProducts = { "iphone X", "Blackberry" };
            String[] actualProducts = new string[2];

            LoginPage lp = new LoginPage(getDriver());
            ProductsPage productsPage = lp.validLogin(username, password);

            productsPage.waitUntilPageLoads();

            IList<IWebElement> products = productsPage.getAllCards();
            foreach (IWebElement product in products)
            {
                String prodName = product.FindElement(productsPage.getCardTitle()).Text;
                TestContext.Progress.WriteLine(prodName);
                if (expProducts.Contains(prodName))
                {
                    product.FindElement(productsPage.getAddToCartBtn()).Click();
                }

            }

            checkoutPage cp = productsPage.clickCheckoutBtn();

            IList<IWebElement> checkoutCards = cp.getCards();

            for (int i = 0; i < checkoutCards.Count; i++)
            {
                actualProducts[i] = checkoutCards[i].Text;
            }

            Assert.AreEqual(expProducts, actualProducts);

            confirmationPage confPage = cp.checkOut();

            confPage.getLocationTxtbox().SendKeys(location.Substring(0,3));

            TestContext.Progress.WriteLine(location.Substring(0, 3));

            confPage.selectLocation(location);

            confPage.acceptTermsAndConditions();

            confPage.purchase();

            String message = confPage.getActualMessage().Text;

            StringAssert.Contains("Success", message);

        }

        public static IEnumerable<TestCaseData> AddTestcaseDataConfig()
        {
            yield return new TestCaseData(getDataParser().extractData("username"), getDataParser().extractData("password"),getDataParser().extractDataArray("products"), getDataParser().extractData("location"));
            yield return new TestCaseData(getDataParser().extractData("username"), getDataParser().extractData("password"), getDataParser().extractDataArray("products"), getDataParser().extractData("location"));
            yield return new TestCaseData(getDataParser().extractData("username_wrong"), getDataParser().extractData("password_wrong"), getDataParser().extractDataArray("products"), getDataParser().extractData("location"));
        }

        [Test, Category("Regression")]
        public void Test1()
        {
            driver.Value.Url = "https://sso.teachable.com/secure/9521/identity/login";
            driver.Value.FindElement(By.Id("email")).SendKeys("BushraNaqvi");
            driver.Value.FindElement(By.Name("password")).SendKeys("Password");
            driver.Value.FindElement(By.XPath("//input[@value='Login']")).Click();

            Thread.Sleep(3000);

            String actualError = driver.Value.FindElement(By.ClassName("auth-flash-error")).Text;
            TestContext.Progress.WriteLine("Actual error is: " + actualError);

            String expectedError = "Your email or password is incorrect.";
            TestContext.Progress.WriteLine("Exoected error is: " + expectedError);

            Assert.AreEqual(expectedError, actualError);

        }
    }
}
