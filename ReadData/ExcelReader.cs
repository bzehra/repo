﻿
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.Util;
using NPOI.XSSF.UserModel;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumFramework.ReadData
{

    class ExcelReader
    {

        public void getSheetData(String sheetName)
        {
            string filePath = @"C:\Users\bzehr\work\ExcelData\TestData.xlsx";
            try
            {
                IWorkbook workbook = null;
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                if (filePath.IndexOf(".xlsx") > 0)
                    workbook = new XSSFWorkbook(fs);
                else if (filePath.IndexOf(".xls") > 0)
                    workbook = new HSSFWorkbook(fs);

                int noOfSheets = workbook.NumberOfSheets;
                for (int i = 0; i < noOfSheets; i++)
                {
                    if (workbook.GetSheetName(i).Equals(sheetName))
                    {
                        ISheet sheet = workbook.GetSheetAt(i);
                        if (sheet != null)
                        {
                            int rowCount = sheet.LastRowNum;

                            for (int j = 1; j <= rowCount; j++)
                            {
                                IRow currentRow = sheet.GetRow(j);
                                int lastCol = currentRow.LastCellNum;
                                for (int k = 0; k < lastCol ; k++)
                                {
                                    if (currentRow.GetCell(k).CellType == CellType.String)
                                    {
                                        var cellValue = currentRow.GetCell(k).StringCellValue.Trim();
                                        TestContext.Progress.Write(cellValue + "\t");
                                    }
                                    else if (currentRow.GetCell(k).CellType == CellType.Numeric)
                                    {
                                        var cellValue = currentRow.GetCell(k).NumericCellValue;
                                        TestContext.Progress.Write(cellValue + "\t");
                                    }
                                }
                                TestContext.Progress.WriteLine();
                            }

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
        }

        /*        [Test]
                public void ReadExcelSheet()
                {
                    string path = @"C:\Users\bzehr\work\ExcelData\TestData.xlsx";
                    XSSFWorkbook workbook = new XSSFWorkbook(File.Open(path, FileMode.Open));
                    var sheet = workbook.GetSheetAt(0);
                    var row = sheet.GetRow(0);
                    var value = row.GetCell(0).StringCellValue.Trim();

                    Debug.WriteLine(value);

                }*/
    }
}

